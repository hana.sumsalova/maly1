def is_palindrome(data):
    reverse_l = list(data)
    reverse_l.reverse()
    reverse_l =  ''.join(reverse_l)
    if (data == reverse_l):
        return True
    else:
        return False

def count_successive(string):
    if string == '':
        return []
    l = []
    a = string[0]
    i = 1
    for char in string[1:]:
        if char == a:
            i += 1
        else:
            l.append((a, i))
            i = 1
            a = char
    l.append((a, i))
    return l

def find_positions(items):
    if items == []:
        return {}
    a = items[0]
    dic = {}
    l = []
    for c in items:
        i = 0
        l = []
        for char in items:
            if char == c:
                l.append(i)
            i += 1
        dic.update({c:l})
    return dic

def invert_dictionary(dictionary):
    d = list(dictionary.values())
    l1 = len(d)
    d = set(d)
    l2 = len(d)
    if l1 != l2:
        return None
    print(l1, l2)
    print(d)
    dictionary = {y: x for x, y in dictionary.items()}
    return dictionary


def lex_compare(a, b):
    i = min(len(a), len(b))
    c = 0
    for char in range(0,i):
        if(ord(a[c]) > ord(b[c])):
            return b
        if(ord(a[c]) < ord(b[c])):
            return a
        else:
            c +=1
    
    if(len(a) > len(b)):
        return b
    else:
        return a


